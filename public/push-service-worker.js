self.addEventListener('push', function(event) {
    console.log('[Service Worker] Push Received.');
    console.log(`[Service Worker] Push had this data: "${event.data.text()}"`);
    console.log(event.data.json());

    var data = event.data.json()

    const title = data.title;
    const options = {
      body: data.body,
      icon: 'favicon.ico',
      badge: 'icon512x512.png'
    };
    event.waitUntil(self.registration.showNotification(title, options));
  });

  console.log("SW running smoothly!");
