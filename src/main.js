import Vue from 'vue'
import App from './App.vue'
import axios from 'axios'
import './registerServiceWorker'

Vue.use(require('vue-moment'));

// Need to add production url
axios.defaults.baseURL = (process.env.NODE_ENV === 'production') ? `https://coffeeapi.verynice.party` : `http://${window.location.hostname}:3006/`
console.log("Using API at: " + axios.defaults.baseURL)

Vue.config.productionTip = false

new Vue({
  render: h => h(App),
}).$mount('#app')
