import axios from 'axios'
import moment from 'moment'

export default {
  getUpcoming() {
    return axios.get('/events/upcoming.json')
      .then(response => {
        response.data.forEach(function (event) {
          event.start_date = new Date(event.start_date)
        })
        return response.data
      })
  }
}
