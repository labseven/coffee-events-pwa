import axios from 'axios'

function urlB64ToUint8Array(base64String) {
  const padding = '='.repeat((4 - base64String.length % 4) % 4);
  const base64 = (base64String + padding)
    .replace(/-/g, '+')
    .replace(/_/g, '/');

  const rawData = window.atob(base64);
  const outputArray = new Uint8Array(rawData.length);

  for (let i = 0; i < rawData.length; ++i) {
    outputArray[i] = rawData.charCodeAt(i);
  }
  return outputArray;
}

var ServerKey = 'BMLEB6wduI-vScr5LzuxUCgWe4f_41wdRtDIx5pfojtZYOJuWgwY8yk1OKVuGxgtZQAvNePx8TLme6OTFEQlvBs=' // key from https://web-push-codelab.glitch.me/ or firebase
var ServerKeyUint8 = urlB64ToUint8Array(ServerKey)

export default {
  subscribe(serviceWorker) {

    return new Promise(function(resolve, reject) {
      Notification.requestPermission().then(() => {
        console.log("done requestPermission")
        serviceWorker.pushManager.subscribe({
            userVisibleOnly: true,
            applicationServerKey: ServerKeyUint8
        }).then((subscription) => {
            var keys = JSON.parse(JSON.stringify(subscription)).keys
            axios.post('/subscribers.json', {
              endpoint: subscription.endpoint,
              p256dh: keys.p256dh,
              auth: keys.auth
            }) //.then((response) => console.log(response));
            console.log("subscription success")
            resolve(subscription)
        }).catch((err) => {
            console.log("subscription error", err);
            reject()
        })
      })
    });
  },

  unsubscribe(serviceWorker) {
    return new Promise(function(resolve, reject) {
      if (serviceWorker) {
        serviceWorker.pushManager.getSubscription().then((subscription) => {
        axios.post('/subscribers/unsubscribe.json', {
              endpoint: subscription.endpoint
            }) //.then((response) => console.log(response));
        resolve(subscription.unsubscribe())
      })
      } else {
        reject()
     }
    })
  }
}
