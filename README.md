# Coffee Events PWA

The Progressive Web App (PWA) frontend for Coffee Events.

Built with Vue.

The server is [here](https://gitlab.com/labseven/coffee-events/).


## Todo:
* ~~Make layout nicer~~
* ~~Change colors~~
* ~~Add logo and manifest~~
* ~~Change how push notifications are shown~~


# How to Vue

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```
